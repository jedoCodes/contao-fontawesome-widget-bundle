# Contao FontAwesome IconPicker BackendWidget

[![Latest Stable Version](https://poser.pugx.org/jedolabs/contao-fontawesome-widget-bundle/v/stable.svg)](https://packagist.org/packages/jedolabs/contao-fontawesome-widget-bundle) 
[![Total Downloads](https://poser.pugx.org/jedolabs/contao-fontawesome-widget-bundle/downloads.svg)](https://packagist.org/packages/jedolabs/contao-fontawesome-widget-bundle) 
[![Latest Unstable Version](https://poser.pugx.org/jedolabs/contao-fontawesome-widget-bundle/v/unstable.svg)](https://packagist.org/packages/jedolabs/contao-fontawesome-widget-bundle) 
[![License](https://poser.pugx.org/jedolabs/contao-fontawesome-widget-bundle/license.svg)](https://packagist.org/packages/jedolabs/contao-fontawesome-widget-bundle)

## About

This bundle adds Font Awesome icon widget functionality to contao. This makes managing icons a breeze.
This allows you to create input fields that allow you to add Font Awesome icons to their articles.


### Installation

* Installation Guide (EN): [INSTALLATION_EN.md](INSTALLATION_EN.md)
* Installationsanleitung (DE): [INSTALLATION_DE.md](INSTALLATION_DE.md)


### User guide (Coming Soon!)

* English: https://docs.jedo-labs.de/en/fontawesome-widget-bundle.html
* German: https://docs.jedo-labs.de/de/fontawesome-widget-bundle.html


### field integration example
 
 ```php
 $GLOBALS['TL_DCA']['tl_myExtension']['fields']['myField'] = array(
     'label' => &$GLOBALS['TL_LANG']['tl_myExtension']['myField'],
     'search' => true,
     'inputType' => 'FontAwesomeWidget',
     'eval' => array('doNotShow' => true),
     'sql' => "blob NULL",
 );
```
