<?php

/**
 * @copyright  jedoLabs 2018 <https://jedo-labs.de>
 * @author     Jens Doberenz (WurzelGnOOm)
 * @package    jedolabs/fontawesome-icon-picker-bundle
 * @license    LGPL-3.0+
 * @see	       https://gitlab.com/jedolabs/fontawesome-iconpicker-bundle
 *
 */

namespace JedoLabs\ContaoFontawesomeWidgetBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;


/**
 * Configures the fontawesome icon picker bundle.
 *
 * @author Jens Doberenz <https://gitlab.com/jedolabs/>
 */
class ContaoFontawesomeWidgetBundle extends Bundle
{


    public function getFontAwesomeJavascript()
    {

        $FontAwesomeJsSRC = ContaoFontawesomeWidgetBundle::getSelectedFontAwesomeSRC();

        return $FontAwesomeJsSRC."/js/all.min.js";

    }

    public function getFontAwesomeIconYaml()
    {

        $FontAwesomeSRC = ContaoFontawesomeWidgetBundle::getSelectedFontAwesomeSRC();


        return $FontAwesomeSRC."/metadata/icons.yml";

    }

    public function getSelectedFontAwesomeSRC()
    {

        $objFile = \FilesModel::findById(\Contao\Config::get('FIP_FontAwesomeSRC'));

        return "assets/fontawesome-free";

    }

}
